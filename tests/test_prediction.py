import unittest
from unittest import mock
from api import app
import json
import freezegun
import os.path
import calendar
import datetime
from pytz import timezone

context_path = app.config['CONTEXT_ROOT']
api_category = app.config['API_CATEGORY']
message_repository_dir_path = app.config['MESSAGE_REPOSITORY_DIR_PATH']

base_uri = '/' + context_path + '/' + api_category


class TestPredictionApi(unittest.TestCase):
    """api.prediction()のUnitTestCase
    """

    def setUp(self):
        """テスト前処理
        """

        self.body = {
            'message': 'べんきょうだいすき！'
        }
        self.test_app = app.test_client()
        self.logfile1Path = ''
        self.logfile2Path = ''

    def tearDown(self):
        """テスト後処理。テストにて生成されたログファイルを削除する。
        """
        if self.logfile1Path != '' and os.path.exists(self.logfile1Path):
            os.remove(self.logfile1Path)
        if self.logfile2Path != '' and os.path.exists(self.logfile2Path):
            os.remove(self.logfile2Path)

    def test_prediction(self):
        """テスト内容: 投稿メッセージ判定APIがOKを返すこと

        予想結果: 以下のレスポンスが返却されること
            Content-Type: application/json
            Response Body:
                {
                    "id": "3e8f562b-5ff9-4b05-beb5-defc62ee1e51",
                    "label": "OK",
                    "confidence": 0.629,
                    "reason": "",
                    "method": "AI1"
                }
        """
        body = {
            'message': 'べんきょうだいすき！'
        }
        response = self.test_app.post(
            base_uri + '/prediction',
            data=json.dumps(body), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        res = json.loads(response.data)
        self.assertEqual(res['label'], 'OK')
        self.assertEqual(res['confidence'], 0.629)
        self.assertEqual(res['reason'], '')
        self.assertEqual(res['method'], 'AI1')
        self.assertTrue(logContextValidate('', response.data, body['message']))

    def test_prediction_check_ContentType(self):
        """テスト内容: [Content-Type]が未設定の場合、投稿メッセージ判定APIを呼び出して、チェックエラーが返却されること。

        予想結果: 以下のレスポンスが返却されること
            Content-Type: application/problem+json
            Status: 415
            Response Body:
                {
                    "title": "Unsupported Media Type",
                    "status": 415,
                    "detail": "Content-Type:application/json is required"
                }
        """
        response = self.test_app.post(
            base_uri + '/prediction',
            data=json.dumps(self.body))
        self.assertEqual(response.status_code, 415)
        res = json.loads(response.data)
        self.assertEqual(res['title'], 'Unsupported Media Type')
        self.assertEqual(
            res['detail'], 'Content-Type:application/json is required')

    def test_message_check(self):
        """テスト内容: 投稿メッセージ文字の最大桁数の境界値テスト。
        桁数が1000文字時と1001文字時を検証する。

        予想結果:
        ・桁数==1000の場合
            以下のレスポンスが返却されること(桁数==1000)
            Content-Type: application/json
            Response Body:
                {
                    "id": "3e8f562b-5ff9-4b05-beb5-defc62ee1e51",
                    "label": "OK",
                    "confidence": 0.629,
                    "reason": "",
                    "method": "AI1"
                }
        ・桁数==1001の場合
            以下のレスポンスが返却されること
            Content-Type: application/problem+json
            Status: 400
            Response Body:
                {
                    "title": "Validation Error",
                    "status": 400,
                    "detail": "message is too long.' +
                    'message can be up to 1000 characters."
                }
        """
        message = ''
        # メッセージデータ準備
        for i in range(1000):
            message = message + 'き'
        body = {
            'message': message
        }
        response = self.test_app.post(
            base_uri + '/prediction',
            data=json.dumps(body), content_type='application/json')
        assert response.status_code == 200
        res = json.loads(response.data)
        self.assertEqual(res['label'], 'OK')
        self.assertEqual(res['confidence'], 0.629)
        self.assertEqual(res['reason'], '')
        self.assertEqual(res['method'], 'AI1')

        # messageが1001文字の場合
        body = {
            'message': message + '1'
        }
        response = self.test_app.post(
            base_uri + '/prediction',
            data=json.dumps(body), content_type='application/json')
        assert response.status_code == 400
        res = json.loads(response.data)
        self.assertEqual(res['title'], 'Validation Error')
        self.assertEqual(res['status'], 400)
        self.assertEqual(
            res['detail'],
            'message is too long.message can be up to 1000 characters.')

    def test_prediction_status415(self):
        """テスト内容: サポートされていないContent-Typeである（Content-Type: text）を指定し、投稿メッセージ判定APIを呼び出す

        予想結果: 以下のレスポンスが返却されること
            Content-Type: application/problem+json
            Status: 415
            Response Body:
                {
                    "title": "Unsupported Media Type",
                    "status": 415,
                    "detail": "The server does not support the media type
                     transmitted in the request."
                }
        """
        body = {
            'message': 'べんきょうだいすき！'
        }
        response = self.test_app.post(
            base_uri + '/prediction',
            data=json.dumps(body), content_type='text')
        self.assertEqual(response.status_code, 415)
        res = json.loads(response.data)
        self.assertEqual(res['title'], 'Unsupported Media Type')
        self.assertEqual(res['status'], 415)
        self.assertEqual(
            res['detail'], 'The server does not support the media type '
            + 'transmitted in the request.')

    def test_prediction_status400(self):
        """テスト内容: 必須項目未入力の場合、投稿メッセージ判定APIを呼び出して、チェックエラーが返却されること。

        予想結果: 以下のレスポンスが返却されること
            Content-Type: application/problem+json
            Status: 400
            Response Body:
                {
                    "title": "Validation Error",
                    "status": 400,
                    "detail": "message is required"
                }
        """
        body = {}
        response = self.test_app.post(
            base_uri + '/prediction',
            data=json.dumps(body), content_type='application/json')
        self.assertEqual(response.status_code, 400)
        res = json.loads(response.data)
        title = 'Validation Error'
        detail = 'message is required'
        self.assertEqual(res['title'], title)
        self.assertEqual(res['detail'], detail)

    @mock.patch('api.postanalytics.MessageAnalyzer.predict')
    def test_prediction_message_analyzer_RuntimeError(self, mock_get):
        """テスト内容: 投稿メッセージ判定APIが呼び出したModelの処理でRuntimeError例外が発生する。

        予想結果: 以下のレスポンスが返却されること
            Content-Type: application/problem+json
            Status: 500
            Response Body:
                {
                    "title": "Internal Server Error",
                    "status": 500,
                    "detail": "The server encountered an internal error '
                    + 'and was unable to complete your request.  '
                    + 'Either the server is overloaded '
                    + 'or there is an error in the application."
                }
        """
        body = {
            'message': 'べんきょうだいすき！'
        }
        mock_get.side_effect = RuntimeError()
        response = self.test_app.post(
            base_uri + '/prediction',
            data=json.dumps(body), content_type='application/json')
        self.assertEqual(response.status_code, 500)
        res = json.loads(response.data)
        title = 'Internal Server Error'
        self.assertEqual(res['title'], title)
        detail = 'The server encountered an internal error ' + \
            'and was unable to complete your request. ' + \
            'Either the server is overloaded ' + \
            'or there is an error in the application.'
        self.assertEqual(res['detail'], detail)

    def test_prediction_logCheck(self):
        """テスト内容: 投稿メッセージ判定APIを呼び出し、メッセージをメッセージ記録ファイルへ出力する。

        予想結果:
            ・ログが1行で出力されていること
            ・連続でリクエストがきた場合、同じファイルに出力されること
            ・ログファイル名がシステム日付のyyyy-mm.logとなること
            ・APIの結果が、以下の通り正しく返却されること。
            Content-Type: application/json
                Response Body:
                    {
                        "id": "3e8f562b-5ff9-4b05-beb5-defc62ee1e51",
                        "label": "OK",
                        "confidence": 0.629,
                        "reason": "",
                        "method": "AI1"
                    }
        """
        logfilepath = app.config['MESSAGE_REPOSITORY_DIR_PATH'] + '/'
        sourcedate = datetime.date.today()
        months = 1
        yearMonth1 = '{0:%Y-%m}'.format(add_months(sourcedate, months))
        while os.path.isfile(logfilepath + yearMonth1 + '.log'):
            months = months + 1
            yearMonth1 = '{0:%Y-%m}'.format(add_months(sourcedate, months))

        months = months + 1
        yearMonth2 = '{0:%Y-%m}'.format(add_months(sourcedate, months))
        while os.path.isfile(logfilepath + yearMonth2 + '.log'):
            months = months + 1
            yearMonth2 = '{0:%Y-%m}'.format(add_months(sourcedate, months))

        self.logfile1Path = logfilepath + yearMonth1 + '.log'
        freezer1 = freezegun.freeze_time(yearMonth1 + '-01')
        freezer1.start()
        try:
            response = self.test_app.post(
                base_uri + '/prediction',
                data=json.dumps(self.body), content_type='application/json')
            self.assertTrue(logContextValidate(
                self.logfile1Path, response.data, self.body['message']))
            response = self.test_app.post(
                base_uri + '/prediction',
                data=json.dumps(self.body), content_type='application/json')
            self.assertTrue(logContextValidate(
                self.logfile1Path, response.data, self.body['message']))

        finally:
            freezer1.stop()

        self.logfile2Path = logfilepath + yearMonth2 + '.log'
        freezer2 = freezegun.freeze_time(yearMonth2 + '-01')
        freezer2.start()
        try:
            response = self.test_app.post(
                base_uri + '/prediction',
                data=json.dumps(self.body), content_type='application/json')
            self.assertTrue(logContextValidate(
                self.logfile2Path, response.data, self.body['message']))
        finally:
            freezer2.stop()

        self.assertTrue(os.path.isfile(self.logfile1Path))
        self.assertTrue(os.path.isfile(self.logfile2Path))

        response = self.test_app.post(
            base_uri + '/prediction',
            data=json.dumps(self.body), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        res = json.loads(response.data)
        self.assertEqual(res['label'], 'OK')
        self.assertEqual(res['confidence'], 0.629)
        self.assertEqual(res['reason'], '')
        self.assertEqual(res['method'], 'AI1')
        self.assertTrue(logContextValidate(
            '', response.data, self.body['message']))

    @mock.patch('os.path.abspath')
    def test_prediction_logger_FileNotFoundError(self, mock_get):
        """テスト内容: 投稿メッセージ判定APIがメッセージの書き込みに失敗してFileNotFoundError例外が発生するが、レスポンスは正常に返却される。

        予想結果:
            ・メッセージが書き込まれていないこと
            ・以下のレスポンスが返却されること
                Content-Type: application/problem+json
                Status: 200
                Response Body:
                    {
                        "id": "3e8f562b-5ff9-4b05-beb5-defc62ee1e51",
                        "label": "OK",
                        "confidence": 0.629,
                        "reason": "",
                        "method": "AI1"
                    }
        """
        logfilepath = app.config['MESSAGE_REPOSITORY_DIR_PATH'] + '/'
        sourcedate = datetime.date.today()
        months = 1
        yearMonth1 = '{0:%Y-%m}'.format(add_months(sourcedate, months))
        while os.path.isfile(logfilepath + yearMonth1 + '.log'):
            months = months + 1
            yearMonth1 = '{0:%Y-%m}'.format(add_months(sourcedate, months))
        logfile1Path = logfilepath + yearMonth1 + '.log'
        freezer1 = freezegun.freeze_time(yearMonth1 + '-01')
        freezer1.start()
        try:
            mock_get.return_value = 'dummyPath' + logfile1Path
            response = self.test_app.post(
                base_uri + '/prediction',
                data=json.dumps(self.body), content_type='application/json')

            self.assertEqual(response.status_code, 200)
            res = json.loads(response.data)
            self.assertEqual(res['label'], 'OK')
            self.assertEqual(res['confidence'], 0.629)
            self.assertEqual(res['reason'], '')
            self.assertEqual(res['method'], 'AI1')
        finally:
            freezer1.stop()


def logContextValidate(logfilepath,  data, message):
    """logfilepathの最終行が、dataの内容と一致するか判定する。

    Args:
        logfilepath (str): ログファイルパス。ブランクの場合は、システム日付からログファイルのパスを生成する。
        data (str): レスポンス情報
        message (str): リクエスト用メッセージ内容

    Returns:
        bool: True 一致 False 不一致
    """
    if logfilepath == '':
        logName = '{0:%Y-%m}'.format(
            datetime.datetime.now(timezone('Asia/Tokyo')))
        logfilepath = app.config['MESSAGE_REPOSITORY_DIR_PATH'] + '/'
        logfilepath = logfilepath + logName + '.log'
    with open(logfilepath, 'r', encoding='utf-8_sig') as f1:
        last_line = json.loads(f1.readlines()[-1])
        res = json.loads(data)
        lastlogContext = '{"id": "' + last_line['id'] + \
            '", "label": "' + last_line['label'] + \
            '", "confidence": ' + str(last_line['confidence']) + \
            ', "reason": "", "method": "' + last_line['method'] +\
            '", "timestamp": "' + last_line['timestamp'] + \
            '", "message": "' + last_line['message'] + \
            '"}\n'
        resLogContext = '{"id": "' + res['id'] + \
            '", "label": "' + res['label'] + \
            '", "confidence": ' + str(res['confidence']) + \
            ', "reason": "", "method": "' + res['method'] +\
            '", "timestamp": "' + last_line['timestamp'] + \
            '", "message": "' + message + \
            '"}\n'
        return lastlogContext == resLogContext


def add_months(sourcedate, months):
    """月を加減する。

    Args:
        sourcedate (date): 基準となる日付
        months (int): 追加する月数

    Returns:
        加減後の日付を返却する。

    Examples:
        sourcedate＝20181231の場合 => 返却日付＝20190131になること。
        sourcedate＝20190131の場合 => 返却日付＝20190228になること。
    """
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month // 12
    month = month % 12 + 1
    day = min(sourcedate.day, calendar.monthrange(year, month)[1])
    return datetime.date(year, month, day)


if __name__ == '__main__':
    unittest.main()
