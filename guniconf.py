import multiprocessing

# Worker Processes
workers = 1
worker_class = "sync"
timeout = 600
threads = 2

# Logging
loglevel = "info"
logconfig = None
