from setuptools import setup, find_packages


setup(
    name="api:app",
    version="1.0.0",
    description="Deploy to Gunicorn container",
    packages=find_packages(),
    install_requires=["gunicorn"],
)
