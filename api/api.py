import json
import logging
import os
import uuid
from datetime import datetime

from flask import abort, jsonify, make_response, request

from api import app, message_analyzer
from pytz import timezone


logger = app.logger
context_path = app.config['CONTEXT_ROOT']
api_category = app.config['API_CATEGORY']


msg_logger = logging.getLogger('msgLog')
msg_logger.setLevel(logging.INFO)
current_message_file = ''


@app.route('/' + context_path + '/' + api_category + '/healthz',
           methods=['GET'])
def healthz():
    """healthcheck API

    メッセージ格納ディレクトリ($MESSAGE_REPOSITORY_DIR_PATH)が利用不可の場合はERRORログを出力しstatus:WARNを返却する。
    それ以外はstatus:OKを返却する。

    Request:
        Method: GET
        Request Body:
            No parameters

    Response:
        Content-Type: application/json
        Response Body:
            status(string): healthcheckの結果。OK,WARN,NG。
            message(string): 詳細を表すメッセージ。
        Response Body Example:
        {
            "status": "OK",
            "message": "API is running"
        }

    """
    status = 'OK'
    message = 'API is running'
    if not os.path.exists(app.config['MESSAGE_REPOSITORY_DIR_PATH']):
        status = 'WARN'
        message = 'Messages can not be written. ' + \
            'There is a possibility that the NFS server is down.'
        logger.error(message)
    return jsonify({'status': status, 'message': message})

@app.route('/' + context_path + '/' + api_category + '/prediction',
           methods=['POST'])
def prediction():
    """投稿メッセージ判定API

    投稿メッセージの適切性判定結果を取得する。
    判定結果は以下パスのメッセージ記録ファイルに1行で追記する。

    ${MESSAGE_REPOSITORY_DIR_PATH}/messages_${API_CATEGORY}.log

    メッセージの書き込みに失敗した場合は、ERRORログを出力し、正常に結果を返却する。

    MessageFileFormat:
        id (string($uuid)): リクエスト毎に採番されるユニークID
        label (string): 判定結果。OK,NG,UC。
        confidence(float): 判定結果の確信度。
        reason(string): NG、UCの理由 (合致ルール名など)
        method(string): 最終判定を下したエンジン
        timestamp(string): 書き込み日時
        message(string): メッセージ

        Message File Example:
            {"id": "31895a22-f936-45ad-9145-28f6d6a3b7b8", "label": "OK", "confidence": 0.629, "reason": "", "method": "AI1", "timestamp": "2019-03-07T16:27:02.105784+09:00", "message": "べんきょうだいすき！"}

    Request:
        Method: POST
        Content-Type: application/json
        Request Body:
            message(string): 投稿メッセージ文字列。1文字~1000文字まで。

        Request Body Example:
        {
            "message": "このまえのテストのけっかをおしえてください"
        }

    Response:
        Content-Type: application/json
        Response Body:
            id (string($uuid)): リクエスト毎に採番されるユニークID
            label (string): 判定結果。OK,NG,UC。
            confidence(float): 判定結果の確信度。
            reason(string): NG、UCの理由 (合致ルール名など)
            method(string): 最終判定を下したエンジン

        Response Body Example:
        {
            "id": "3e8f562b-5ff9-4b05-beb5-defc62ee1e51",
            "label": "OK",
            "confidence": 0.629,
            "reason": "",
            "method": "AI1"
        }


    """
    if 'Content-Type' not in request.headers.keys():
        return __createProblemResponse('Unsupported Media Type', 415,
                                       'Content-Type:application/json ' +
                                       'is required')

    if 'application/json' not in request.headers['Content-Type']:
        return abort(415)

    if 'message' not in request.json or not request.json['message']:
        return __createProblemResponse('Validation Error', 400,
                                       'message is required')

    message = request.json['message']

    if len(message) > 1000:
        return __createProblemResponse('Validation Error',
                                       400,
                                       'message is too long.' +
                                       'message can be up to 1000 characters.')

    try:
        prediction = message_analyzer.predict(message)
    except RuntimeError as e:
        logger.exception('Postanalytics Modelの呼び出しで予期しないエラーが発生しました。', e)
        return abort(500)

    result = {
        'id': str(uuid.uuid4()),
        'label': prediction.label,
        'confidence': prediction.confidence,
        'reason': prediction.reason,
        'method': prediction.method
    }

    msg_record = result.copy()
    msg_record['timestamp'] = datetime.now(timezone('Asia/Tokyo')).isoformat()
    msg_record['message'] = message

    try:
        now = '{0:%Y-%m.log}'.format(
            datetime.now(timezone('Asia/Tokyo')))
        global current_message_file
        if now != current_message_file:
            current_message_file = now
            for h in msg_logger.handlers:
                msg_logger.removeHandler(h)
            msg_logger.addHandler(logging.FileHandler(
                app.config['MESSAGE_REPOSITORY_DIR_PATH'] +
                '/' + current_message_file,
                encoding='utf-8'))
        msg_logger.info(json.dumps(msg_record, ensure_ascii=False))
    except Exception as e:
        logger.exception('messageの書き込みに失敗しました。id=[%s]' % result['id'], e)

    return jsonify(result)


@app.errorhandler(400)
@app.errorhandler(404)
@app.errorhandler(405)
@app.errorhandler(415)
@app.errorhandler(500)
@app.errorhandler(502)
@app.errorhandler(503)
def default_error_handler(error):
    """エラーハンドラ

    エラー発生時の共通的なReponseを生成するハンドラ

    Args:
        error (flask.error): エラーオブジェクト

    Returns:
        flask.Response: : エラー用Responseを返却する(application/problem+json)。

        Response Body example:

        {
            "title": "Method Not Allowed",
            "status": 405,
            "detail": "The method is not allowed for the requested URL."
        }

    """

    return __createProblemResponse(error.name, error.code, error.description)


def __createProblemResponse(title, status, detail):
    """ProblemResonse生成

    RFC7807 Problem Details for HTTP APIs に準拠したResponseを生成する。
    URL: https://tools.ietf.org/html/rfc7807

    Args:
        title (str): エラーのサマリ情報
        status (str): HTTP status code
        detail (str): エラーの詳細

    Returns:
        flask.Response: : エラー用Responseを返却する(application/problem+json)。

        Response Body example:

        {
            "title": "Method Not Allowed",
            "status": 405,
            "detail": "The method is not allowed for the requested URL."
        }
    """
    response = make_response(
        jsonify({'title': title, 'status': status, 'detail': detail}), status)
    response.headers['Content-Type'] = 'application/problem+json'
    return response
