import logging
import time
from typing import NamedTuple

from . import modelinterface

logger = logging.getLogger(__name__)


class Prediction(NamedTuple):
    label: str
    confidence: float
    reason: str
    method: str


class MessageAnalyzer(modelinterface.ModelInterface):
    def __init__(self):
        """AI判定に必要なモジュールをロードするコンストラクタ。

        """

        logger.warn('DefaultのMessageAnalyzerを使用します。※本番環境では使用禁止です！')

    def predict(self, message):
        """40秒待機(スリープ)して、投稿メッセージ判定結果を返却する

        Args:
            message (str): 投稿メッセージ

        Returns:
            Prediction: 判定結果
        """
        time.sleep(40)
        return Prediction('OK', 0.629, '', 'AI1')
