import logging

from flask import Flask

from model import postanalytics


app = Flask(__name__)
app.config.from_object('config.ApiConfig')

format = '%(levelname)s: %(name)s: %(filename)s#%(lineno)d: %(message)s'
logging.basicConfig(level=getattr(
    logging, app.config['LOG_LEVEL'].upper(), None), format=format)

logger = logging.getLogger(__name__)
logger.info('Postanalytics Model のロードを開始します。')
message_analyzer = postanalytics.MessageAnalyzer()
logger.info('Postanalytics Model のロードが完了しました。')


def create_app():
    app.config["JSON_AS_ASCII"] = False
    app.config["JSON_SORT_KEYS"] = False
    logger.info('API Configs')
    logger.info(app.config)
    from api import api
    logger.info('API is started.')


create_app()
