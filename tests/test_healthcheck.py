import unittest
from unittest import mock
from api import app
import json

context_path = app.config["CONTEXT_ROOT"]
api_category = app.config["API_CATEGORY"]
message_repository_dir_path = app.config["MESSAGE_REPOSITORY_DIR_PATH"]

base_uri = '/' + context_path + '/' + api_category


class TestHealthzApi(unittest.TestCase):
    """api.healthz()のUnitTestCase
    """

    def setUp(self):
        """テスト前処理
        """
        self.test_app = app.test_client()

    def test_healthcheck(self):
        """テスト内容: ヘルスチェックがOKを返すこと。

        予想結果: 以下のレスポンスが返却されること
            Content-Type: application/json
            Status: 200
            Response Body:
                {
                    "status": "OK",
                    "message": "API is running"
                }
        """
        response = self.test_app.get(base_uri + '/healthz')
        assert response.status_code == 200
        res = json.loads(response.data)
        self.assertEqual(res['status'], 'OK')
        self.assertEqual(res['message'], 'API is running')

    @mock.patch('os.path.exists')
    def test_healthcheck_pathExistsCheck(self, mock_get):
        """テスト内容: メッセージ格納ディレクトリ($MESSAGE_REPOSITORY_DIR_PATH)が利用不可の場合、ヘルスチェックを呼び出す。

        予想結果: 以下のレスポンスが返却されること
            Content-Type: application/json
            Status: 200
            Response Body:
                {
                    "status": "WARN",
                    "message": "Messages can not be written.
                    There is a possibility that the NFS server is down."
                }
        """
        mock_get.return_value = False
        response = self.test_app.get(base_uri + '/healthz')
        assert response.status_code == 200
        res = json.loads(response.data)
        status = 'WARN'
        message = 'Messages can not be written. ' + \
            'There is a possibility that the NFS server is down.'
        self.assertEqual(res['status'], status)
        self.assertEqual(res['message'], message)


if __name__ == '__main__':
    unittest.main()
