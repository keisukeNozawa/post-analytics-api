from setuptools import find_packages, setup

setup(
    name='model',
    version='0.1',
    description='Default package for post-analytics',
    author='dcs',
    packages=find_packages(),
)