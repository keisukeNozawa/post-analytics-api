from abc import ABCMeta, abstractmethod


class ModelInterface(metaclass=ABCMeta):

    # モデル読み込み
    @abstractmethod
    def __init__(self):
        pass

    # 判定処理
    @abstractmethod
    def predict(self, content):
        pass
