# OpenAPI

API仕様書は[OpenAPISpec](https://github.com/OAI/OpenAPI-Specification)に準拠する。  
APIドキュメントは[redoc-cli](https://github.com/Rebilly/ReDoc/blob/master/cli/README.md)で生成する。

## Requirement

[Node.js](https://nodejs.org/ja/)

## Install

```
npm install -g redoc-cli
```

proxyなどでinstallできない場合は以下を実行する。
```
npm -g config set proxy "http://gienah.in.dcs.co.jp:8080"
npm -g config set https-proxy "http://gienah.in.dcs.co.jp:8080"
npm config set strict-ssl false
git config --global http.sslVerify false
# インストール後にtureに戻すこと
```

## API Document

```
cd OpenAPI
redoc-cli bundle -o PostAnalyticsAPI.html openapi.yaml
```