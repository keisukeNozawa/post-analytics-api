# Postman

APIテストツールの[Postman](https://www.getpostman.com/)を利用する。

## Requirement

[Node.js](https://nodejs.org/ja/) >= v6

## Install

|ツール|説明|
|---|---|
|[Postman](https://www.getpostman.com/)|テストスクリプトの開発、実行を行う|
|[newman](https://www.npmjs.com/package/newman)|Postmanのコマンドラインツール|
|[newman-reporter-html](https://www.npmjs.com/package/newman-reporter-html)|HTMLレポート生成ツール|
|[newman-reporter-htmlextra](https://www.npmjs.com/package/newman-reporter-htmlextra)|リッチなHTMLレポートを生成するための拡張ツール|

## Test flow

### テストスクリプト開発

Postmanにてスクリプトの開発、実行を行う。  
テスト環境依存の設定は以下のファイルに定義する。
|ファイル名|テスト環境|
|---|---|
|globals_DCS_dev.json|DCS開発環境|
|globals_AWS_APIGateway.json|AWS APIGateway(本番環境)|

### テストスクリプト実行(CLI)

```拡張HTMLレポート生成
cd Postman
set HTTP_PROXY=http://gienah.in.dcs.co.jp:8080
set HTTPS_PROXY=http://gienah.in.dcs.co.jp:8080
set NO_PROXY=cognitive.openshift.in.dcs.co.jp,localhost

newman run PostanalyticsAPITest.postman_collection.json --insecure --folder RegressionTest --insecure --globals globals_DCS_dev.json --reporters cli,htmlextra --reporter-htmlextra-darkTheme --reporter-htmlextra-export PostAnalyticsAPITest_darkTheme.html
```