import os


class ApiConfig(object):
    """APIのConfigを一元管理する

    設定値を当クラスの変数として保持することでflask.app.configに設定される。
    環境変数は当クラスにて設定する。

    Examples:
        設定値の取得方法は以下の通り。

        context_path = app.config['CONTEXT_ROOT']

    """

    LOG_LEVEL = os.getenv('LOG_LEVEL', 'INFO')
    CONTEXT_ROOT = os.getenv('CONTEXT_ROOT', 'api/v1')
    API_CATEGORY = os.getenv('API_CATEGORY', 'elementary')
    MESSAGE_REPOSITORY_DIR_PATH = os.getenv(
        'MESSAGE_REPOSITORY_DIR_PATH', 'logs')
